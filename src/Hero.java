import java.util.ArrayList;

public class Hero extends Entity{

    private String name; //name of hero
    ArrayList<Treasure> Treasures = new ArrayList<>(); //creates a new arraylist for hero's treasures

    public Hero(String name, int posX, int posY) { //overloaded constructor for hero
        this.name = name; //sets name of hero
        super.setPosition(posX, posY); //sets position of hero
    }

    public String getName() { //returns hero name
        return name;
    }

    public void attack(Entity i) { //hero's attack method
        if(i instanceof Dragon) { //detects if dragon
            if(((Dragon) i).getColor() == "Golden") {
                System.out.println(name + " bravely defeated the Golden dragon and came away with gold coins as a prize.");
                Treasures.add(Treasure.Coins); //if golden dragon hero gets coins
            }
            else {
                System.out.println(name + " bravely defeated the Red dragon."); //any other color dragon
            }
        }
        else if(i instanceof Crate) { //detect if crate
            System.out.println(name + " crushed the crate into bits and found" + ((Crate) i).getTreasure());
            Treasures.add(((Crate) i).getTreasure()); //gets the treasure from crate
        }
    }

    public ArrayList<Treasure> getTreasures() { //returns the hero's treasure array
        return Treasures;
    }

    @Override
    public String toString() { //prints a string and position
        return (name + " is standing at (" + x + ", " + y + ")");
    }
}
