public class Dragon extends Entity{
    private String color; //variable color

    public Dragon(String color, int posX, int posY){ //overloaded constructor for dragon
        this.color = color;
        super.setPosition(posX, posY);
    }

    public String getColor() { //returns color of dragon
        return this.color;
    }

    @Override
    public String toString() { //returns a string with dragons position
        return ("The " + color + " dragon breathing fire at (" + x + ", " + y + ")");
    }
}
