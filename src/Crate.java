public class Crate extends Entity{
    private Treasure item; //variable treasure that crate holds

    public Crate (Treasure item, int posX, int posY) { //crate overloaded constructor
        this.item = item;
        super.setPosition(posX, posY);
    }

    public Treasure getTreasure() { //returns the item in crate
        return item;
    }

    @Override
    public String toString() { //returns a string with crates position
        return ("Crate with " + item + " at (" + x + ", " + y + ")");
    }
}
