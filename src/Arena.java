import java.util.ArrayList;

public class Arena {

    private int yLen; //arena x and y coords
    private int xLen;

    ArrayList<Entity[]> grid;
    Hero Hero;

    Entity[] xAxis;
    public Arena(int sizeX, int sizeY) { //overloaded constructor
        this.xLen = sizeX;
        this.yLen = sizeY;
        xAxis = new Entity[sizeY];
        grid = new ArrayList<>();
        for(int i = 0; i < sizeX; i++){ //creates an arraylist of arrays
            grid.add(new Entity[sizeY]);
        }
    }

    public ArrayList<Entity[]> getGrid() { //returns the arraylist
        return grid;
    }

    public boolean add(Entity ent) {
        if(ent instanceof Hero && Hero == null) { //adds entity to arena and reports if successful
            Hero = (Hero) ent;
            grid.get(ent.getPosition().x)[ent.getPosition().y] = ent;
            System.out.println("Successfully added" + " '" + Hero.getName() + " standing at " + Hero.getPosition().toString() + "' " + "to the game arena.");
            return false;
        }
        else if ((grid.get(ent.getPosition().x)[ent.getPosition().y]) == null && !(ent instanceof Hero)) {
            System.out.println("Successfully added" + " '" + ent.toString() + "' " + "to the game arena.");
            grid.get(ent.getPosition().x)[ent.getPosition().y] = ent;
            return true;
        }
        else {
            if(ent instanceof Hero) {
                System.out.println("Could not add" + " '" + ent.toString() + "' " + "because there is already a hero in the arena.");
            }
            else {
                System.out.println("Could not add" + " '" + ent.toString() + "' " + "because another entity is already there.");
            }
            return false;
        }
    }

    public Hero getHero() { //returns the hero entity
        return Hero;
    }

    public void moveHero(int x, int y) { //moves the hero
        if(grid.get(x)[y] != null) {
            Hero.attack(grid.get(x)[y]);
        }
        grid.get(Hero.getPosition().x)[Hero.getPosition().y] = null;
        grid.get(x)[y] = Hero;
        Hero.setPosition(x, y);
        System.out.println(Hero.getName() + "moved to " + Hero.getPosition().toString());
    }

    public int getEntityCount() { //finds out the total entities on arena
        int count = 0;
        for(int i = 0; i < xLen; i++) {
            for(int j = 0; j < yLen; j++) {
                if(grid.get(i)[j] != null) {
                    count++;
                }
            }
        }
        return count;
    }

    public int getDragonCount() { //finds total dragon on arena
        int count = 0;
        for(int i = 0; i < xLen; i++) {
            for(int j = 0; j < yLen; j++) {
                if(grid.get(i)[j] instanceof Dragon) {
                    count++;
                }
            }
        }
        return count;
    }

    public int getTreasureCount(Treasure type) { //finds total treasures on arena
        int count = 0;
        for(int i = 0; i < xLen; i++) {
            for(int j = 0; j < yLen; j++) {
                if(grid.get(i)[j] instanceof Crate && ((Crate) grid.get(i)[j]).getTreasure() == type) {
                    count++;
                }
            }
        }
        return count;
    }

    public void reportHero() { //prints out the hero's treasures and position
        System.out.println("--- Hero report for" + Hero.getName() + " ---");
        System.out.println("Position: " + Hero.getPosition().toString());
        System.out.println("Treasures:");
        for (Treasure tres: Hero.getTreasures()) {
            System.out.println("   " + tres);
        }
        System.out.println();
    }
}
