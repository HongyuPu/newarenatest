public class Position {
    public int x; //x and y position variable
    public int y;

    public Position (int posX, int posY) { //sets parameter position to x and y
        x = posX;
        y = posY;
    }

    public String toString() {
        String posString = ("(" + x + ", " + y + ")"); //prints out a string version
        return posString;
    }
}
