public class Entity { //super class to hero, crate and dragon
    public int x;
    public int y; //x and y integers for position

    public Entity() { //stand in default constructor

    }

    public Entity(int posX, int posY) { //overloaded constructor
        x = posX;
        y = posY; //sets x and y variables
    }

    public Position getPosition() {
        Position pos = new Position(x, y); //returns an entity's position
        return pos;
    }

    public void setPosition(int posX, int posY) { //sets an entity's position
        this.x = posX;
        this.y = posY;
    }

}
